﻿using System;
using System.Collections.Generic;

namespace RadiantAlphaBotCore.RagnarokOnline
{
    public class RROOnlineData
    {
        public bool success { get; set; }
        public string message { get; set; }
        public List<RROBasicCharacter> online { get; set; }
    }

    public class RROBasicCharacter
    {
        public string char_name { get; set; }
        public string char_class { get; set; }
        public int base_level { get; set; }
        public int job_level { get; set; }
        public string guild_name { get; set; }
        public string last_map { get; set; }
    }
}
