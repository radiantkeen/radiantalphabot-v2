﻿using System;
using System.Threading.Tasks;
using Discord.Commands;

namespace RadiantAlphaBotCore.Preconditions
{
    public class DevChannelOnly : PreconditionAttribute
    {
       public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            return Task.FromResult(context.Channel.Id != Program.config.specialChannel["botdev"]
                ? PreconditionResult.FromError("")
                : PreconditionResult.FromSuccess());
        }
    }
}
