﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

namespace RadiantAlphaBotCore.Preconditions
{
    public class AdminOnly : PreconditionAttribute
    {
        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            return Task.FromResult((context.User as IGuildUser)?.GuildPermissions.Administrator != true
                ? PreconditionResult.FromError("This command is Admin only.")
                : PreconditionResult.FromSuccess());
        }
    }
}