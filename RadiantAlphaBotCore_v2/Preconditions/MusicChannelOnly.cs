﻿using System;
using System.Threading.Tasks;
using Discord.Commands;

namespace RadiantAlphaBotCore.Preconditions
{
    public class MusicChannelOnly : PreconditionAttribute
    {
        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            return Task.FromResult((context.Channel.Id != Program.config.specialChannel["musiccmd"] && context.Channel.Id != Program.config.specialChannel["botdev"])
                ? PreconditionResult.FromError("This command only works in the #musicdrops channel.")
                : PreconditionResult.FromSuccess());
        }
    }
}