﻿using System;

namespace RadiantAlphaBotCore
{
    public class RABotRoleMapping
    {
        public string roleName { get; set; }
        public string description { get; set; }
        public string emojiname { get; set; }
    }
}
