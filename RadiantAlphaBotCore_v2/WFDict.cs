﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using System.Linq;
using System.Timers;
using System.Threading.Tasks;
using System.Net.Http;
using Discord.WebSocket;

namespace RadiantAlphaBotCore.Warframe
{
    public static class WFDict
    {
        public static List<string> wfDataList;
        public static Dictionary<string, string> Faction;
        public static Dictionary<string, string> Syndicate;
        public static Dictionary<string, string> MissionType;
        public static Dictionary<string, WFNode> Nodes;
        public static Dictionary<string, string> SortieModifier;
        public static Dictionary<string, string> SortieModifierDesc;
        public static Dictionary<string, WFSortieBoss> SortieBoss;
        public static Dictionary<string, WFLotusPathItem> WFLotusPath;
        private static HttpClient webClient;

        static WFDict()
        {
            webClient = Program.webClient;

            Faction = new Dictionary<string, string>
            {
                ["FC_CORPUS"] = "Corpus",
                ["FC_CORRUPTED"] = "Corrupted",
                ["FC_GRINEER"] = "Grineer",
                ["FC_INFESTATION"] = "Infested",
                ["FC_OROKIN"] = "Orokin",
                ["FC_SENTIENT"] = "Sentient"
            };

            wfDataList = new List<string>
            {
                "https://raw.githubusercontent.com/WFCD/warframe-worldstate-data/master/data/syndicatesData.json",
                "https://raw.githubusercontent.com/WFCD/warframe-worldstate-data/master/data/missionTypes.json",
                "https://raw.githubusercontent.com/WFCD/warframe-worldstate-data/master/data/solNodes.json",
                "https://raw.githubusercontent.com/WFCD/warframe-worldstate-data/master/data/sortieData.json",
                "https://raw.githubusercontent.com/WFCD/warframe-items/development/data/json/All.json"
            };

            Program.globalTickSecond.Elapsed += CalculateCycles;
        }

        public static string ParseTime(string theTime, int dstOffset = -5)
        {
            if (long.TryParse(theTime, out long outTime))
                return ParseTime(outTime, dstOffset);

            return "unknown";
        }

        public static string ParseTime(long inTime, int dstOffset = -5)
        {
            try
            {
                DateTimeOffset parsedTime = DateTimeOffset.FromUnixTimeMilliseconds(inTime).AddHours(dstOffset);
                return parsedTime.ToString("MMM dd yyyy HH:mm:ss");
            }
            catch
            {
                return "unknown";
            }
        }

        public static string ParseTimeLeft(string theTime, int dstOffset = -5)
        {
            if (long.TryParse(theTime, out long outTime))
                return ParseTimeLeft(outTime, dstOffset);

            return "unknown";
        }

        public static string ParseTimeLeft(long inTime, int dstOffset = -5)
        {
            try
            {
                DateTimeOffset parsedTime = DateTimeOffset.FromUnixTimeMilliseconds(inTime).AddHours(dstOffset);
                TimeSpan timeLeft = parsedTime - DateTime.UtcNow.AddHours(dstOffset);

                //if(timeLeft.TotalDays < 1.0)
                    return timeLeft.ToString("c");
                //else
                    //return timeLeft.ToString("c");
            }
            catch(Exception e)
            {
                Console.WriteLine("err thrown: " + e.ToString());
                return "unknown";
            }
        }

        private static void CalculateCycles(object source, ElapsedEventArgs e)
        {
            

            Console.WriteLine($"Background task ticked [{DateTime.Now.ToString("hh:mm:ss")}]");
        }

        public static async Task UpdateDataAsync()
        {
            foreach(string url in wfDataList)
            {
                string fileName = url.Split("/").Last();
                var response = await webClient.GetAsync(url);

                if (!response.IsSuccessStatusCode)
                    continue;

                using(var fs = File.Create(fileName))
                    await response.Content.CopyToAsync(fs);
            }

            LoadData();
        }

        public static void LoadData()
        {
            Nodes = JsonConvert.DeserializeObject<Dictionary<string, WFNode>>(File.ReadAllText("solNodes.json"));
            WFSortieData tmpSortie = JsonConvert.DeserializeObject<WFSortieData>(File.ReadAllText("sortieData.json"));
            SortieModifier = tmpSortie.modifierTypes;
            SortieModifierDesc = tmpSortie.modifierDescriptions;
            SortieBoss = tmpSortie.bosses;
            Dictionary<string, WFMissionType> tmpMissionTypes = JsonConvert.DeserializeObject<Dictionary<string, WFMissionType>>(File.ReadAllText("missionTypes.json"));
            Dictionary<string, WFSyndicateType> tmpSyndicates = JsonConvert.DeserializeObject<Dictionary<string, WFSyndicateType>>(File.ReadAllText("syndicatesData.json"));
            List<WFInternalPathRef> tmpLotusPathData = JsonConvert.DeserializeObject<List<WFInternalPathRef>>(File.ReadAllText("All.json"));
            MissionType = new Dictionary<string, string>();
            Syndicate = new Dictionary<string, string>();
            WFLotusPath = new Dictionary<string, WFLotusPathItem>();

            foreach (KeyValuePair<string, WFMissionType> mt in tmpMissionTypes)
                MissionType[mt.Key] = mt.Value.value;

            foreach (KeyValuePair<string, WFSyndicateType> syn in tmpSyndicates)
                Syndicate[syn.Key] = syn.Value.name;

            foreach (WFInternalPathRef lp in tmpLotusPathData)
                WFLotusPath[lp.uniqueName.Split("/").Last()] = new WFLotusPathItem()
                {
                    name = lp.name,
                    description = lp.description,
                    imageName = lp.imageName
                };
        }
    }

    public class WFNode
    {
        public string value { get; set; }
        public string enemy { get; set; }
        public string type { get; set; }
    }

    public class WFSortieBoss
    {
        public string name { get; set; }
        public string faction { get; set; }
    }

    public class WFSortieData
    {
        public Dictionary<string, string> modifierTypes;
        public Dictionary<string, string> modifierDescriptions;
        public Dictionary<string, WFSortieBoss> bosses;
    }

    public class WFMissionType
    {
        public string value { get; set; }
    }

    public class WFSyndicateType
    {
        public string name { get; set; }
    }

    public class WFLotusPathItem
    {
        public string name { get; set; }
        public string description { get; set; }
        public string imageName { get; set; }
    }

    public class WFInternalPathRef : WFLotusPathItem
    {
        public string uniqueName { get; set; }
    }
}