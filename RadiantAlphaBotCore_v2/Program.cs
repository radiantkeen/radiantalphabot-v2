﻿using System;
using System.IO;
using Newtonsoft.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http;
using System.Timers;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using RadiantAlphaBotCore.Services;
using RadiantAlphaBotCore.Warframe;
using Victoria;

namespace RadiantAlphaBotCore
{
    class Program
    {
        public static RadiantAlphaBotConfig config;
        public static SocketTextChannel raDebugChannel = null;
        public static SocketVoiceChannel raMusicDrops = null;
        public static HttpClient webClient;
        public static WarframeWorldStateData wfState;
        public static Dictionary<ulong, Queue<LavaTrack>> playListCollection;
        private DiscordSocketClient _raclient;
        public static LavaNode lavalinkManager;
        public static System.Timers.Timer globalTickSecond;

        public static void SyncConfig()
        {
            config.roleMap = new Dictionary<string, RABotRoleMapping>();
            config.specialChannel = new Dictionary<string, ulong>();
            config.emoteList = new Dictionary<string, string>();

            using (MySqlConnection conn = new MySqlConnection(config.mysqlConnStr))
            {
                conn.Open();

                using (MySqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = String.Format("SELECT * FROM ralpha.alphabot_channelmap WHERE serverid = {0}", config.adminGuildId);

                    MySqlDataReader reader = cmd.ExecuteReader();

                    while(reader.Read())
                        config.specialChannel[reader["name"].ToString()] = Convert.ToUInt64(reader["channelid"]);
                }

                using (MySqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = String.Format("SELECT * FROM ralpha.alphabot_emotes WHERE serverid = {0}", config.adminGuildId);

                    MySqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                        config.emoteList[reader["emotename"].ToString()] = reader["filename"].ToString();
                }

                using (MySqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = String.Format("SELECT * FROM ralpha.alphabot_rolemap WHERE serverid = {0}", config.adminGuildId);

                    MySqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        RABotRoleMapping role = new RABotRoleMapping
                        {
                            roleName = reader["rolename"].ToString(),
                            description = reader["description"].ToString(),
                            emojiname = reader["emoji"].ToString()
                        };

                        config.roleMap[role.roleName] = role;
                    }
                }
            }
        }

         public static async Task<WarframeWorldStateData> wfGetOrDownloadAsync(bool bForce = false)
        {
            if (!(wfState is null) && !bForce && DateTime.Now < wfState.freshTime)
                return wfState;

            globalTickSecond.Enabled = false;

            try
            {
                wfState = WarframeWorldStateData.FromJson(await webClient.GetStringAsync("http://content.warframe.com/dynamic/worldState.php"));
                wfState.earthCycle = JsonConvert.DeserializeObject<WFStatCycleDataEarth>(await webClient.GetStringAsync("https://api.warframestat.us/pc/cetusCycle"));
                wfState.venusCycle = JsonConvert.DeserializeObject<WFStatCycleDataVenus>(await webClient.GetStringAsync("https://api.warframestat.us/pc/vallisCycle"));
            }
            catch
            {
                globalTickSecond.Enabled = true;
                throw;
            }

            //globalTickSecond.Enabled = true;

            return wfState;
        }

        static void Main(string[] args)
        {
            webClient = new HttpClient();
            playListCollection = new Dictionary<ulong, Queue<LavaTrack>>();
            wfState = null;
            globalTickSecond = new System.Timers.Timer(1000.0);

            WFDict.LoadData();

            try
            {
                LoadConfig();
                SyncConfig();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Database error: {0} - Restart the process in a few seconds.", ex.Message);
                Thread.Sleep(5000);
                Environment.Exit(1);
            }
            catch (FileNotFoundException)
            {
                config = new RadiantAlphaBotConfig();

                Console.WriteLine("Configuration file not found, generating new one and closing the process.");
                SyncConfig();
                SaveConfig();
                Thread.Sleep(5000);
                Environment.Exit(1);
            }

            //globalTickSecond.Enabled = true;

            new Program().MainAsync().GetAwaiter().GetResult();
        }

        public static void SaveConfig()
        {
            File.WriteAllText("config.json", JsonConvert.SerializeObject(config, Formatting.Indented));
        }

        public static void LoadConfig()
        {
            config = JsonConvert.DeserializeObject<RadiantAlphaBotConfig>(File.ReadAllText("config.json"));
        }

        public async Task MainAsync()
        {
            _raclient = new DiscordSocketClient(new DiscordSocketConfig
            {
                LogLevel = LogSeverity.Info
            });

            var provider = ConfigureServices();
            provider.GetRequiredService<LogService>(); // creates an instance
            await provider.GetRequiredService<CommandHandlingService>().InitializeAsync(provider);
            lavalinkManager = provider.GetRequiredService<LavaNode>();

            await _raclient.LoginAsync(TokenType.Bot, config.authKey);
            await _raclient.StartAsync();
            await _raclient.SetGameAsync("Radiant Alpha Support Bot");

            _raclient.Ready += OnConnect;

            await Task.Delay(-1);
        }

        // Logged in, do this thing
        private async Task OnConnect()
        {
            raDebugChannel = _raclient.GetGuild(config.adminGuildId).GetTextChannel(config.specialChannel["botdev"]);
            raMusicDrops = _raclient.GetGuild(config.adminGuildId).GetVoiceChannel(config.specialChannel["voice"]);

            // Create music playlists for all connected guilds - originally scrapped but brought back because Victoria is stupid with playlists
            foreach(var guild in _raclient.Guilds)
                if(!playListCollection.ContainsKey(guild.Id))
                    playListCollection[guild.Id] = new Queue<LavaTrack>();

            wfState = await wfGetOrDownloadAsync(true);

            await lavalinkManager.ConnectAsync();
            await raDebugChannel.SendMessageAsync($"Bot logged in at {DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}");
        }

        private IServiceProvider ConfigureServices()
        {
            ServiceCollection services = new ServiceCollection();

            var voiceConfig = new LavaConfig
            {
                Hostname = config.lavalinkIPAddr,
                Port = 2333,
                Authorization = config.lavalinkPassword,
                LogSeverity = LogSeverity.Verbose,
                SelfDeaf = false
            };

            var victoria = new LavaNode(_raclient, voiceConfig);

            services.AddSingleton(_raclient);
            services.AddSingleton<CommandService>();
            services.AddSingleton<CommandHandlingService>();
            services.AddLogging(configure => configure.AddConsole()).Configure<LoggerFilterOptions>(options => options.MinLevel = LogLevel.Information);
            services.AddSingleton<LogService>();
            services.AddSingleton(victoria);

            return services.BuildServiceProvider();
        }
    }
}
