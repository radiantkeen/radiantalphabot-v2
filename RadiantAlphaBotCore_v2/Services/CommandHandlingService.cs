﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Victoria;
using Victoria.EventArgs;

namespace RadiantAlphaBotCore.Services
{
    public class CommandHandlingService
    {
        private readonly DiscordSocketClient _discord;
        private readonly CommandService _commands;
        private readonly LavaNode _lavalinkManager;
        private IServiceProvider _provider;
        private RadiantAlphaBotConfig config;
        private Dictionary<ulong, Queue<LavaTrack>> playListCollection;

        public CommandHandlingService(IServiceProvider provider, DiscordSocketClient discord, CommandService commands, LavaNode lavalinkManager)
        {
            _discord = discord;
            _commands = commands;
            _provider = provider;
            _lavalinkManager = lavalinkManager;
            config = Program.config;
            playListCollection = Program.playListCollection;

            _discord.MessageReceived += MessageReceived;
            _discord.ReactionAdded += BotClientReactionAdded;
            _discord.ReactionRemoved += BotClientReactionRemoved;

            _lavalinkManager.OnTrackEnded += VictoriaOnTrackEnded;
            _lavalinkManager.OnTrackException += VictoriaOnTrackException;
            _lavalinkManager.OnTrackStuck += VictoriaOnTrackStuck;
            //_lavalinkManager.OnWebSocketClosed += VictoriaOnWebSocketClosed;
            //_lavalinkManager.OnPlayerUpdated += VictoriaOnPlayerUpdated;
            //_lavalinkManager.OnStatsReceived += VictoriaOnStatsReceived;
        }

        public async Task InitializeAsync(IServiceProvider provider)
        {
            _provider = provider;
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), provider);
            // Add additional initialization code here...

        }

        private async Task MessageReceived(SocketMessage rawMessage)
        {
            // Ignore system messages and messages from bots
            if (!(rawMessage is SocketUserMessage message)) return;
            if (message.Source != MessageSource.User) return;

            int argPos = 0;
            if (!message.HasCharPrefix(Program.config.botListenChar[0], ref argPos) && !message.HasMentionPrefix(_discord.CurrentUser, ref argPos))
                return;

            var context = new SocketCommandContext(_discord, message);
            var result = await _commands.ExecuteAsync(context, argPos, _provider);

            if (result.Error.HasValue &&
                result.Error.Value != CommandError.UnknownCommand)
                await context.Channel.SendMessageAsync(result.ToString());
        }

        public async Task BotClientReactionAdded(Cacheable<IUserMessage, ulong> cachedMessage, ISocketMessageChannel originChannel, SocketReaction reaction)
        {
            if (Program.config.roleMessageId == 0)
                return;

            var message = await cachedMessage.GetOrDownloadAsync();

            if (message == null)
                return;

            if (message.Id != Program.config.roleMessageId)
                return;

            string reqRoleName = null;

            foreach (var maprole in Program.config.roleMap.Values)
            {
                if (maprole.emojiname == reaction.Emote.Name)
                {
                    reqRoleName = maprole.roleName;
                    break;
                }
            }

            if (reqRoleName is null)
                return;

            SocketGuild thisGuild = (originChannel as SocketGuildChannel).Guild;
            SocketGuildUser target = thisGuild.GetUser(reaction.UserId);

            if (target is null)
            {
                await Program.raDebugChannel.SendMessageAsync($"Failed to detect user who reacted to role message, reaction: {reaction.Emote}{reaction.Emote.Name}");
                return;
            }

            SocketRole reqRole = null;

            foreach (SocketRole role in thisGuild.Roles)
            {
                if(role.Name == reqRoleName)
                {
                    reqRole = role;
                    break;
                }
            }

            if (reqRole is null)
            {
                await Program.raDebugChannel.SendMessageAsync($"Failed to resolve role object for emote {reaction.Emote}{reaction.Emote.Name} from {target.Nickname}");
                return;
            }


            foreach (SocketRole role in target.Roles)
            {
                if(role == reqRole)
                {
                    await Program.raDebugChannel.SendMessageAsync($"Failed to add role for {target.Nickname} - already has role");
                    return;
                }       
            }

            try
            {
                await target.AddRoleAsync(reqRole);
            }
            catch(Exception e)
            {
                await Program.raDebugChannel.SendMessageAsync($"Error applying role {reaction.Emote}{reqRole.Name} to {target.Nickname}: {e.Message}");
            }
        }

        public async Task BotClientReactionRemoved(Cacheable<IUserMessage, ulong> cachedMessage, ISocketMessageChannel originChannel, SocketReaction reaction)
        {
            if (Program.config.roleMessageId == 0)
                return;

            var message = await cachedMessage.GetOrDownloadAsync();

            if (message == null)
                return;

            if (message.Id != Program.config.roleMessageId)
                return;

            string reqRoleName = null;

            foreach (var maprole in Program.config.roleMap.Values)
            {
                if (maprole.emojiname == reaction.Emote.Name)
                {
                    reqRoleName = maprole.roleName;
                    break;
                }
            }

            if (reqRoleName is null)
                return;

            SocketGuild thisGuild = (originChannel as SocketGuildChannel).Guild;
            SocketGuildUser target = thisGuild.GetUser(reaction.UserId);

            if (target is null)
            {
                await Program.raDebugChannel.SendMessageAsync($"Failed to detect user who unreacted to role message, reaction: {reaction.Emote}{reaction.Emote.Name}");
                return;
            }

            SocketRole reqRole = null;

            foreach (SocketRole role in thisGuild.Roles)
            {
                if (role.Name == reqRoleName)
                {
                    reqRole = role;
                    break;
                }
            }

            if (reqRole is null)
                return;

            foreach (SocketRole role in target.Roles)
            {
                if (role == reqRole)
                {
                    try
                    {
                        await target.RemoveRoleAsync(reqRole);
                    }
                    catch (Exception e)
                    {
                        await Program.raDebugChannel.SendMessageAsync($"Error removing role {reaction.Emote}{reqRole.Name} from {target.Nickname}: {e.Message}");
                    }

                    return;
                }
            }
        }

        public async Task VictoriaOnReadyAsync() => await _lavalinkManager.ConnectAsync();

        private Task VictoriaOnPlayerUpdated(PlayerUpdateEventArgs arg)
        {
            Console.WriteLine($"Player update received for {arg.Player.VoiceChannel.Name}.");
            return Task.CompletedTask;
        }

        private Task VictoriaOnStatsReceived(StatsEventArgs arg)
        {
            Console.WriteLine($"Lavalink Uptime {arg.Uptime}.");
            return Task.CompletedTask;
        }

        private async Task VictoriaOnTrackEnded(TrackEndedEventArgs args)
        {
            /*if (!args.Reason.ShouldPlayNext())
                return;*/

            var player = args.Player;
            
            if (playListCollection[player.VoiceChannel.Guild.Id].Any())
            {
                var track = playListCollection[player.VoiceChannel.Guild.Id].Dequeue();

                await args.Player.PlayAsync(track);

                var nowPlayingEmbed = new EmbedBuilder
                {
                    Description = $"👉 **{player.Track.Title}**",
                    Color = new Color(213, 0, 249),
                    Title = "Now Playing"
                }.Build();

                //await player.StopAsync();
                //await player.PlayAsync(track);
                //await ReplyAsync(null, false, playing);
                await args.Player.TextChannel.SendMessageAsync(null, false, nowPlayingEmbed);
            }
            else
                await args.Player.TextChannel.SendMessageAsync("Finished playing tracks");
        }

        private async Task VictoriaOnTrackException(TrackExceptionEventArgs arg)
        {
            await Program.raDebugChannel.SendMessageAsync($"Victoria threw exception for Track {arg.Track.Title}: {arg.ErrorMessage}");
            //_logger.LogCritical($"Track exception received for {arg.Track.Title}.");
            //return Task.CompletedTask;
        }

        private async Task VictoriaOnTrackStuck(TrackStuckEventArgs arg)
        {
            await Program.raDebugChannel.SendMessageAsync($"Victoria reports Track {arg.Track.Title} is stuck");
            //_logger.LogError($"Track stuck received for {arg.Track.Title}.");
            //return Task.CompletedTask;
        }

        private async Task VictoriaOnWebSocketClosed(WebSocketClosedEventArgs arg)
        {
            await Program.raDebugChannel.SendMessageAsync($"Victoria reports Discord connection closed: {arg.Reason}");
            Console.WriteLine($"Victoria reports Discord connection closed: {arg.Reason}");
            //_logger.LogCritical($"Discord WebSocket connection closed with following reason: {arg.Reason}");
            //return Task.CompletedTask;
        }

    }
}