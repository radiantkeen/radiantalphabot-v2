﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using RadiantAlphaBotCore.Preconditions;
using Victoria;
using Victoria.Enums;

namespace RadiantAlphaBotCore.Modules
{
    [InVoiceChannel]
    public class MusicCommandModule : ModuleBase<SocketCommandContext>
    {
        private readonly LavaNode _lavalinkManager;
        private static RadiantAlphaBotConfig config = Program.config;
        public static Dictionary<ulong, Queue<LavaTrack>> playListCollection = Program.playListCollection;
        private static SocketVoiceChannel musicDrops;

        public MusicCommandModule(LavaNode lavalinkManager)
        {
            _lavalinkManager = lavalinkManager;
            musicDrops = Program.raMusicDrops;
        }

        [MusicChannelOnly]
        [Command("volume")]
        [Summary("Sets the volume for the current playing song 1-100")]
        public async Task VolumeTask(ushort value = 0)
        {
            _lavalinkManager.TryGetPlayer(Context.Guild, out var player);

            if (player is null)
                player = await _lavalinkManager.JoinAsync(musicDrops, Context.Channel as ITextChannel);

            if (value < 1 || value > 100)
            {
                await ReplyAsync($"Volume is {player.Volume} (range: 1-100)");
                return;
            }

            await player.UpdateVolumeAsync(value);
            await ReplyAsync($"Volume is now {value}");
        }

        [MusicChannelOnly]
        [Command("pause")]
        [Summary("Pauses the song")]
        public async Task PauseTask()
        {
            _lavalinkManager.TryGetPlayer(Context.Guild, out var player);

            if (player is null)
                player = await _lavalinkManager.JoinAsync(musicDrops, Context.Channel as ITextChannel);

            await player.PauseAsync();
            await ReplyAsync("Paused");
        }

        [MusicChannelOnly]
        [Command("resume")]
        [Alias("Unpause")]
        [Summary("Resumes the song")]
        public async Task ResumeTask()
        {
            _lavalinkManager.TryGetPlayer(Context.Guild, out var player);

            if (player is null)
                player = await _lavalinkManager.JoinAsync(musicDrops, Context.Channel as ITextChannel);

            if (player.PlayerState == PlayerState.Playing)
                return;
            else
            {
                await player.ResumeAsync();
                await ReplyAsync($"Resumed {player.Track.Title}");
            }
        }

        [MusicChannelOnly]
        [Command("playing")]
        [Summary("Get the current playing song")]
        public async Task NowPlayingTask()
        {
            _lavalinkManager.TryGetPlayer(Context.Guild, out var player);

            if (player is null)
                player = await _lavalinkManager.JoinAsync(musicDrops, Context.Channel as ITextChannel);

            if (player.PlayerState != PlayerState.Playing)
            {
                await ReplyAsync("Not currently playing a track");
                return;
            }

            var track = player.Track;
            var embed = new EmbedBuilder
            {
                Title = $"{track.Author} - {track.Title}",
                Url = track.Url
            }
            .AddField("Id", track.Id)
            .AddField("Duration", track.Duration)
            .AddField("Position", track.Position);

            await ReplyAsync(null, false, embed.Build());
        }

        [MusicChannelOnly]
        [Command("clear")]
        [Summary("Clears the queue")]
        public async Task ClearTask()
        {
            _lavalinkManager.TryGetPlayer(Context.Guild, out var player);

            if (player is null)
                player = await _lavalinkManager.JoinAsync(musicDrops, Context.Channel as ITextChannel);

            player.Queue.Clear();
            playListCollection[Context.Guild.Id].Clear();
            await ReplyAsync("Queue cleared");
        }

        [MusicChannelOnly]
        [Command("stop")]
        [Summary("Stops the current playing song.")]
        public async Task StopTask()
        {
            _lavalinkManager.TryGetPlayer(Context.Guild, out var player);

            if (player is null)
                player = await _lavalinkManager.JoinAsync(musicDrops, Context.Channel as ITextChannel);

            await player.StopAsync();
            await ReplyAsync("Stopped playing, the queue is still intact. Use 'clear' to destroy the queue");
        }

        [MusicChannelOnly]
        [Command("disconnect")]
        public async Task LeaveTask()
        {
            _lavalinkManager.TryGetPlayer(Context.Guild, out var player);

            if (player is null)
                player = await _lavalinkManager.JoinAsync(musicDrops, Context.Channel as ITextChannel);

            if (player.PlayerState == PlayerState.Playing)
                await StopTask();

            await _lavalinkManager.LeaveAsync(musicDrops);
        }

        [MusicChannelOnly]
        [Command("playlist")]
        [Summary("Prints the current playlist")]
        public async Task QueueTask()
        {
            _lavalinkManager.TryGetPlayer(Context.Guild, out var player);

            if (player is null)
                player = await _lavalinkManager.JoinAsync(musicDrops, Context.Channel as ITextChannel);

            string my = "";
            var playlist = playListCollection[Context.Guild.Id];

            if (playlist.Count < 1 && player.PlayerState != PlayerState.Playing)
            {
                await ReplyAsync("The queue is empty.");
            }
            else
            {
                if (player.PlayerState == PlayerState.Playing)
                    my += $"👉 [{player.Track.Title}]({player.Track.Url}) **{player.Track.Duration}**\n";

                // Victoria uses some weird proprietary potentially thread-safe linked list setup, which makes this absolutely impossible to do normally
                int idx = 0;

                foreach(var track in playlist)
                {
                    if (idx > 10)
                        break;

                    my += $"**#{idx + 1}**. [{track.Title}]({track.Url}) **{track.Duration}**\n";
                    idx++;
                }
                    
                var build = new EmbedBuilder
                {
                    Title = "Current Queue",
                    Description = my,
                    Color = new Color(213, 0, 249),
                    Footer = new EmbedFooterBuilder
                    {
                        Text = playlist.Count + " tracks in the queue"
                    }
                }.Build();

                await ReplyAsync(null, false, build);
            }
        }

        [MusicChannelOnly]
        [Command("skip")]
        [Summary("Skips the current playing song")]
        public async Task SkipTask()
        {
            _lavalinkManager.TryGetPlayer(Context.Guild, out var player);

            if (player is null)
                player = await _lavalinkManager.JoinAsync(musicDrops, Context.Channel as ITextChannel);

            try
            {
                // This method throws an exception instead of doing something logical or rational
                var track = await player.SkipAsync();

                // Could do this instead to avoid an exception throw, but eh whatever
                //await player.StopAsync();
                //await player.PlayAsync(track);
            }
            catch (Exception e)
            {
                if (player.PlayerState == PlayerState.Playing)
                    await player.StopAsync();

                //await Program.raDebugChannel.SendMessageAsync($"MusicBot Exception thrown: SkipTask -> {e.Message}");
            }
        }

        [MusicChannelOnly]
        [Command("play")]
        [Summary("Plays a song from YouTube or SoundCloud (defaults to YouTube unless a specific URL is given)")]
        public async Task PlayTask([Remainder] string query = null)
        {
            using (Context.Channel.EnterTypingState())
            {
                _lavalinkManager.TryGetPlayer(Context.Guild, out var player);

                if (player is null)
                {
                    player = await _lavalinkManager.JoinAsync(musicDrops, Context.Channel as ITextChannel);
                    await player.UpdateVolumeAsync(25); // Set default volume
                }

                var playlist = playListCollection[Context.Guild.Id];

                if (query is null)
                {
                    if (player.PlayerState == PlayerState.Playing)
                        return;

                    if (playlist.Count > 0)
                    {
                        await player.PlayAsync(playlist.Dequeue());

                        var nowPlayingEmbed = new EmbedBuilder
                        {
                            Description = $"👉 **{player.Track.Title}**",
                            Color = new Color(213, 0, 249),
                            Title = "Now Playing"
                        }.Build();

                        await ReplyAsync(null, false, nowPlayingEmbed);
                    }
                    else
                        await ReplyAsync($"No tracks in queue");

                    return;
                }

                var result = Uri.TryCreate(query, UriKind.Absolute, out var uriResult);
                var identifier = result || query.Contains("ytsearch:") || query.Contains("scsearch:") ? uriResult.ToString() : $"ytsearch:{query}";

                var response = await _lavalinkManager.SearchAsync(identifier);

                if (response.LoadStatus == LoadStatus.LoadFailed)
                {
                    var loadFailEmbed = new EmbedBuilder
                    {
                        Title = "Failed",
                        Description = "This url is not playable",
                        Color = new Color(213, 0, 249),
                        Footer = new EmbedFooterBuilder
                        {
                            Text = "Try adding 'ytsearch:' or 'scsearch:' to search YouTube or SoundCloud respectively"
                        }
                    }.Build();

                    await ReplyAsync(null, false, loadFailEmbed);
                    return;
                }
                else if (response.LoadStatus == LoadStatus.NoMatches)
                {
                    var noMatchesEmbed = new EmbedBuilder
                    {
                        Title = "Failed",
                        Description = "No tracks found",
                        Color = new Color(213, 0, 249),
                        Footer = new EmbedFooterBuilder
                        {
                            Text = "Try adding 'ytsearch:' or 'scsearch:' to search YouTube or SoundCloud respectively"
                        }
                    }.Build();

                    await ReplyAsync(null, false, noMatchesEmbed);

                    return;
                }
                else if (response.LoadStatus == LoadStatus.TrackLoaded || response.LoadStatus == LoadStatus.PlaylistLoaded)
                {
                    var track = response.Tracks[0];

                    if (response.LoadStatus == LoadStatus.PlaylistLoaded)
                    {
                        foreach (var plTrack in response.Tracks)
                            playlist.Enqueue(plTrack);

                        await ReplyAsync($"Added **{response.Tracks.Count}** items to queue.");
                    }
                    else
                    {
                        playlist.Enqueue(track);
                    }

                    if (player.PlayerState != PlayerState.Playing)
                    {
                        await player.PlayAsync(playlist.Dequeue());

                        var nowPlayingEmbed = new EmbedBuilder
                        {
                            Description = $"👉 **{track.Title}**",
                            Color = new Color(213, 0, 249),
                            Title = "Now Playing"
                        }.Build();

                        await ReplyAsync(null, false, nowPlayingEmbed);
                    }
                    else
                        await ReplyAsync($"Added **{track.Title}** to queue.");
                }
                else
                {
                    await Program.raDebugChannel.SendMessageAsync($"MusicBot Exception thrown: Unhandled load type (LoadStatus: {response.LoadStatus})");
                    await ReplyAsync("Whoops, something went wrong!");
                }
            }
        }

        [MusicChannelOnly]
        [Command("queue")]
        [Summary("Adds a track to the queue instead of playing it")]
        public async Task QueueTrackTask([Remainder] string query = null)
        {
            if (query is null)
                return;

            using (Context.Channel.EnterTypingState())
            {
                var result = Uri.TryCreate(query, UriKind.Absolute, out var uriResult);
                var identifier = result || query.Contains("ytsearch:") || query.Contains("scsearch:") ? uriResult.ToString() : $"ytsearch:{query}";

                _lavalinkManager.TryGetPlayer(Context.Guild, out var player);

                if (player is null)
                    player = await _lavalinkManager.JoinAsync(musicDrops, Context.Channel as ITextChannel);

                var playlist = playListCollection[Context.Guild.Id];
                var response = await _lavalinkManager.SearchAsync(identifier);

                if (response.LoadStatus == LoadStatus.LoadFailed)
                {
                    var loadFailEmbed = new EmbedBuilder
                    {
                        Title = "Failed",
                        Description = "This url is not playable",
                        Color = new Color(213, 0, 249),
                        Footer = new EmbedFooterBuilder
                        {
                            Text = "Try adding 'ytsearch:' or 'scsearch:' to search YouTube or SoundCloud respectively"
                        }
                    }.Build();

                    await ReplyAsync(null, false, loadFailEmbed);
                    return;
                }
                else if (response.LoadStatus == LoadStatus.NoMatches)
                {
                    var noMatchesEmbed = new EmbedBuilder
                    {
                        Title = "Failed",
                        Description = "No tracks found",
                        Color = new Color(213, 0, 249),
                        Footer = new EmbedFooterBuilder
                        {
                            Text = "Try adding 'ytsearch:' or 'scsearch:' to search YouTube or SoundCloud respectively"
                        }
                    }.Build();

                    await ReplyAsync(null, false, noMatchesEmbed);

                    return;
                }
                else if (response.LoadStatus == LoadStatus.TrackLoaded || response.LoadStatus == LoadStatus.PlaylistLoaded)
                {
                    var track = response.Tracks[0];

                    playlist.Enqueue(track);
                    await ReplyAsync($"Added **{track.Title}** to queue.");
                }
                else
                {
                    await Program.raDebugChannel.SendMessageAsync($"MusicBot Exception thrown: Unhandled load type (LoadStatus: {response.LoadStatus})");
                    await ReplyAsync("Whoops, something went wrong!");
                }
            }
        }

        [MusicChannelOnly]
        [Command("loadplaylist")]
        [Summary("Loads a playlist into the queue (URL only)")]
        public async Task PlaylistLoadTask(string query = null, bool bOverwrite = false)
        {
            if (query is null)
                return;

            using (Context.Channel.EnterTypingState())
            {
                var result = Uri.TryCreate(query, UriKind.Absolute, out var uriResult);

                _lavalinkManager.TryGetPlayer(Context.Guild, out var player);

                if (player is null)
                    player = await _lavalinkManager.JoinAsync(musicDrops, Context.Channel as ITextChannel);

                var playlist = playListCollection[Context.Guild.Id];
                var response = await _lavalinkManager.SearchAsync(uriResult.ToString());

                if (response.LoadStatus == LoadStatus.LoadFailed)
                {
                    var loadFailEmbed = new EmbedBuilder
                    {
                        Title = "Failed",
                        Description = "This url is not playable",
                        Color = new Color(213, 0, 249),
                        Footer = new EmbedFooterBuilder
                        {
                            Text = "Try adding 'ytsearch:' or 'scsearch:' to search YouTube or SoundCloud respectively"
                        }
                    }.Build();

                    await ReplyAsync(null, false, loadFailEmbed);
                    return;
                }
                else if (response.LoadStatus == LoadStatus.NoMatches)
                {
                    var noMatchesEmbed = new EmbedBuilder
                    {
                        Title = "Failed",
                        Description = "No tracks found",
                        Color = new Color(213, 0, 249),
                        Footer = new EmbedFooterBuilder
                        {
                            Text = "Try adding 'ytsearch:' or 'scsearch:' to search YouTube or SoundCloud respectively"
                        }
                    }.Build();

                    await ReplyAsync(null, false, noMatchesEmbed);

                    return;
                }
                else if (response.LoadStatus == LoadStatus.TrackLoaded || response.LoadStatus == LoadStatus.PlaylistLoaded)
                {
                    if (bOverwrite)
                        playlist.Clear();

                    var trackList = response.Tracks;

                    foreach (var track in trackList)
                        playlist.Enqueue(track);

                    await ReplyAsync($"Added **{trackList.Count}** items to queue.");
                }
                else
                {
                    await Program.raDebugChannel.SendMessageAsync($"MusicBot Exception thrown: Unhandled load type (LoadStatus: {response.LoadStatus})");
                    await ReplyAsync("Whoops, something went wrong!");
                }
            }
        }

    }
}
