﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using System.Net.Http;
using System.Linq;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RadiantAlphaBotCore.Preconditions;
using RadiantAlphaBotCore.Warframe;
using RadiantAlphaBotCore.RagnarokOnline;

namespace RadiantAlphaBotCore.Modules
{
    public class GenericCommandModule : ModuleBase<SocketCommandContext>
    {
        private static RadiantAlphaBotConfig config = Program.config;
        private static WarframeWorldStateData wfState = Program.wfState;
        private static HttpClient webClient = Program.webClient;

        [Command("echo")]
        [Summary("Echoes a message.")]
        public Task cmdecho([Remainder] [Summary("Echoes text")] string echo)
            => ReplyAsync(echo);

        [DevChannelOnly]
        [Command("echodev")]
        [Summary("Echoes a message - dev channel only.")]
        public Task cmdechodev([Remainder] [Summary("Echoes text")] string echo)
            => ReplyAsync(echo);

        [Command("e")]
        [Summary("Uploads an emote to the channel")]
        public async Task PostRAEmote(string emoteText)
        {
            if (emoteText.Length < 1)
                return;

            using (Context.Channel.EnterTypingState())
            {
                string emotePath = null;
                string emoteName = null;

                foreach (string matchedEmote in config.emoteList.Keys)
                {
                    if (emoteText == matchedEmote)
                    {
                        emotePath = config.emoteFilePath + config.emoteList[matchedEmote];
                        emoteName = matchedEmote;
                        break;
                    }
                }

                if (emotePath is null)
                {
                    await Context.Channel.SendMessageAsync($"Emote {emoteText} does not exist.");
                    return;
                }

                using (var imageStream = await webClient.GetStreamAsync(emotePath))
                    await Context.Channel.SendFileAsync(imageStream, config.emoteList[emoteName]);
            }
        }

        [Command("ee")]
        [Summary("Embeds an emote in the channel")]
        public async Task EmbedRAEmote(string emoteText)
        {
            if (emoteText.Length < 1)
                return;

            using (Context.Channel.EnterTypingState())
            {
                string emotePath = null;

                foreach (string matchedEmote in config.emoteList.Keys)
                {
                    if (emoteText == matchedEmote)
                    {
                        emotePath = config.emoteFilePath + config.emoteList[matchedEmote];
                        break;
                    }
                }

                if (emotePath is null)
                {
                    await Context.Channel.SendMessageAsync($"Emote {emoteText} does not exist.");
                    return;
                }

                var embed = new EmbedBuilder().WithImageUrl(emotePath).Build();

                await Context.Channel.SendMessageAsync(null, false, embed);
            }
        }

        [WarframeChannelOnly]
        [Command("wfinfo")]
        [Summary("Shows world state data from Warframe's API")]
        public async Task WFShowWorldStateInfo(string subSection = null, string language = null)
        {
            using (Context.Channel.EnterTypingState())
            {
                try
                {
                    wfState = await Program.wfGetOrDownloadAsync();
                }
                catch (Exception e)
                {
                    await Program.raDebugChannel.SendMessageAsync($"Exception thrown while fetching Warframe World State data: {e.Message}");
                    await Context.Channel.SendMessageAsync("Warframe World State Information API is currently unavailable, try again in a few moments.");
                    return;
                }

                EmbedBuilder wfWSPanelBuilder = new EmbedBuilder();
                string title = null;
                string languageCode = language?.ToLower() ?? "en";

                switch (subSection?.ToLower())
                {
                    case "help":
                        title = "Warframe Info Command Help";
                        wfWSPanelBuilder.AddField("news [region=en]", "Pulls the news feed from the orbiter, including links - you can pull different regions like es, de, jp, etc");
                        wfWSPanelBuilder.AddField("darvo", "Shows the current darvo deal");
                        wfWSPanelBuilder.AddField("alerts", "WIP - will show special alerts like \"Gift of the Lotus\"");
                        wfWSPanelBuilder.AddField("sortie", "Shows the current sortie and all relevant information");
                        wfWSPanelBuilder.AddField("nightwave", "Displays the current nightwave bounties for daily and weekly (sort of)");
                        wfWSPanelBuilder.AddField("anomaly", "Shows the location of the sentient anomaly (if up)");
                        wfWSPanelBuilder.AddField("cycles", "Shows the current status of all day/night cycles (ex. Earth)");
                        wfWSPanelBuilder.AddField("refresh", "Loads fresh data from the Warframe API, admin only");
                        break;

                    case "news":
                        title = "All Warframe News";

                        bool foundMsg;

                        foreach (var wfEvent in wfState.Events)
                        {
                            foundMsg = false;

                            foreach (var newsMessage in wfEvent.Messages)
                            {
                                if (newsMessage.LanguageCode == languageCode)
                                {
                                    wfWSPanelBuilder.AddField(newsMessage.MessageMessage, $"({wfEvent.Prop})");
                                    foundMsg = true;
                                    break;
                                }

                                if (foundMsg)
                                    break;
                            }
                        }

                        break;

                    case "alerts":
                        /*if (wfState.Alerts.Count < 1)
                        {
                            title = "No Warframe Alerts";
                            wfWSPanelBuilder.Description = "No active alerts at this time.";
                            break;
                        }*/

                        int curAlert = 1;
                        string left = null;
                        title = $"{wfState.Alerts.Count} Warframe Alerts";

                        foreach (var alert in wfState.Alerts)
                        {
                            left = WFDict.ParseTimeLeft(alert.Expiry.Date.NumberLong);
                            wfWSPanelBuilder.AddField($"Alert #{curAlert}", $"{WFDict.Faction[alert.MissionInfo.Faction]} {WFDict.MissionType[alert.MissionInfo.MissionType]} ({alert.MissionInfo.MinEnemyLevel}-{alert.MissionInfo.MaxEnemyLevel}) on {WFDict.Nodes[alert.MissionInfo.Location].value} - Time Remaining: {left}");
                            curAlert++;
                        }

                        if(curAlert == 1)
                            wfWSPanelBuilder.AddField($"No Warframe Alerts", "No active alerts at this time");

                        break;

                    case "cycles":
                        var earthCycleRemaining = wfState.earthCycle.expiry.Subtract(DateTime.Now);
                        var venusCycleRemaining = wfState.venusCycle.expiry.Subtract(DateTime.Now);
                        title = "Warframe Day/Night Cycles";

                        wfWSPanelBuilder.AddField("Earth (Cetus)", $"Currently {wfState.earthCycle.state} for {earthCycleRemaining.ToString("c")}");
                        wfWSPanelBuilder.AddField("Venus (Fortuna)", $"Currently {wfState.venusCycle.state} for {venusCycleRemaining.ToString("c")}");
                        break;

                    case "anomaly":
                        var tmpJson = JToken.Parse(wfState.Tmp);

                        if (tmpJson is JArray) // tmp does not currently contain any other data than the anomaly, array is given as a default blank value
                        {
                            if ((tmpJson as JArray).Count < 1)
                            {
                                title = "Anomaly not detected";
                                wfWSPanelBuilder.Description = "Anomaly will appear next in WIP";
                            }
                            else
                            {
                                title = "Multiple anomalies detected";
                                wfWSPanelBuilder.Description = "Looks like worldState schema changed!";
                            }

                            break;
                        }
                        else if (tmpJson is JObject)
                        {
                            dynamic anomaly = tmpJson;

                            int nodeid = anomaly.sfn;
                            string anomalyNode = null;

                            if (nodeid != 0)
                                anomalyNode = WFDict.Nodes[$"CrewBattleNode{nodeid}"].value;

                            if (anomalyNode is null)
                            {
                                title = "Anomaly Detection Error";
                                wfWSPanelBuilder.Description = "Error parsing temp data, worldState was changed";
                            }
                            else
                            {
                                title = "Anomaly Detected";
                                wfWSPanelBuilder.Description = $"Sentient anomaly detected at {anomalyNode}, despawns in WIP!";
                            }
                        }

                        break;

                    case "darvo":
                        var deal = wfState.DailyDeals[0];
                        title = "Darvo's Deal Info"; // potentially can have many deals, but will assume 1 for now
                        long dealEndUT = 0;
                        long.TryParse(deal.Expiry.Date.NumberLong, out dealEndUT);
                        DateTimeOffset dealEndDate = DateTimeOffset.FromUnixTimeMilliseconds(dealEndUT).AddHours(-5);
                        string ddeal = WFDict.WFLotusPath.TryGetValue(deal.StoreItem.Split("/").Last(), out WFLotusPathItem thing) ? thing.name : deal.StoreItem;
                        string dealEndStr = dealEndDate.ToString("MMM dd yyyy HH:mm:ss");

                        wfWSPanelBuilder.AddField($"Selling {ddeal}", $"Cost: ~~{deal.OriginalPrice}p~~ **{deal.SalePrice}p**");
                        wfWSPanelBuilder.AddField("Inventory", $"{(deal.AmountTotal - deal.AmountSold)}/{deal.AmountTotal}", true);
                        wfWSPanelBuilder.AddField("Deal Ends", $"{dealEndStr}", true);
                        break;

                    case "sortie":
                        int stage = 1;
                        var sortie = wfState.Sorties[0];
                        title = "Warframe Current Sortie";

                        wfWSPanelBuilder.WithDescription($"Boss: {WFDict.SortieBoss[sortie.Boss].name}");

                        foreach (var sortieStage in sortie.Variants)
                        {
                            wfWSPanelBuilder.AddField($"Sortie Stage {stage}", $"Mission Type: {WFDict.MissionType[sortieStage.MissionType]} - Modifier: {WFDict.SortieModifier[sortieStage.ModifierType]} - node: {WFDict.Nodes[sortieStage.Node].value} - Tileset: {sortieStage.Tileset}");
                            stage++;
                        }

                        break;

                    case "nightwave":
                        title = "Warframe Nightwave Bounties";

                        string nwStory = WFDict.WFLotusPath.TryGetValue(wfState.SeasonInfo.AffiliationTag, out WFLotusPathItem thing2) ? thing2.name : wfState.SeasonInfo.AffiliationTag;

                        wfWSPanelBuilder.WithDescription($"Story: {nwStory} - Part {wfState.SeasonInfo.Season}, Phase {wfState.SeasonInfo.Phase}");

                        foreach (var nwBounty in wfState.SeasonInfo.ActiveChallenges)
                        {
                            string nwTitle = (nwBounty.Daily ?? false) ? "Daily" : "Weekly";
                            string nwChallenge = nwBounty.Challenge.Split("/").Last();
                            string challenge = WFDict.WFLotusPath.TryGetValue(nwChallenge, out WFLotusPathItem thing3) ? thing3.name : nwChallenge;

                            wfWSPanelBuilder.AddField(nwTitle, challenge, true);
                        }

                        break;

                    case "refresh":
                        if(!((Context.Message.Author as IGuildUser).GuildPermissions.Administrator))
                        {
                            title = "Access Denied";
                            wfWSPanelBuilder.Description = "Only administrators can invoke this command.";
                            break;
                        }

                        title = "Warframe WorldState Refresh";

                        try
                        {
                            if ((DateTime.Now.AddMinutes(1) - wfState.freshTime).TotalSeconds > 1)
                                wfState = await Program.wfGetOrDownloadAsync(true);
                        }
                        catch (Exception e)
                        {
                            await Program.raDebugChannel.SendMessageAsync($"Exception thrown while fetching Warframe World State data: {e.Message}");
                            await Context.Channel.SendMessageAsync("Warframe World State Information API is currently unavailable, try again in a few moments.");
                            return;
                        }

                        wfWSPanelBuilder.WithDescription("Data updated!");
                        break;

                    default:
                        title = "Warframe General Information";
                        wfWSPanelBuilder.Description = $"Use {config.botListenChar}wfinfo help to show a list of commands. This will show general information, like when Baro arrives and the day/night cycles, etc.";
                        break;
                }

                wfWSPanelBuilder.WithAuthor(Context.Client.CurrentUser);
                wfWSPanelBuilder.WithTitle(title ?? "uh oh, something broke");
                wfWSPanelBuilder.WithCurrentTimestamp();
                await Context.Channel.SendMessageAsync(null, false, wfWSPanelBuilder.Build());
            }
        }

        [AdminOnly]
        [Command("wfreload")]
        [Summary("Download and update stored Warframe dictionary data")]
        public async Task WFReload()
        {
            using (Context.Channel.EnterTypingState())
            {
                await WFDict.UpdateDataAsync();
                await Context.Channel.SendMessageAsync("Warframe dictonary data has been reloaded!");
            }
        }

        [Command("q")]
        [Summary("Replies with a quote")]
        public async Task FetchQuote(int id = 0)
        {
            if (id < 0)
                return;

            bool bRand = (id == 0);

            using (Context.Channel.EnterTypingState())
            {
                using (MySqlConnection conn = new MySqlConnection(config.mysqlConnStr))
                {
                    await conn.OpenAsync();

                    using (MySqlCommand cmd = conn.CreateCommand())
                    {
                        if(id == 0)
                        {
                            cmd.CommandText = $"SELECT count(*) as total FROM ralpha.alphabot_quotes";

                            try
                            {
                                using (var max = await cmd.ExecuteReaderAsync())
                                {
                                    max.Read();

                                    int maxId = Convert.ToInt32(max["total"]); // note: if there are 0 quotes, this will cause an infinite loop and throw errors about random(1, 0)

                                    id = (new Random().Next(1, maxId));
                                }
                            }
                            catch (Exception e)
                            {
                                await Program.raDebugChannel.SendMessageAsync($"Quotebot Exception thrown: {e.Message}");
                                await Context.Channel.SendMessageAsync($"Looks like something went wrong, try again in a few moments.");
                                return;
                            }
                        }

                        cmd.CommandText = $"SELECT * FROM ralpha.alphabot_quotes WHERE id = {id}";

                        try
                        {
                            using (var reader = await cmd.ExecuteReaderAsync())
                            {
                                if (!reader.Read())
                                {
                                    if (bRand)
                                    {
                                        await FetchQuote(0); // more hackery, but whatever
                                        return;
                                    }

                                    await Context.Channel.SendMessageAsync($"Quote {id} does not exist.");
                                    return;
                                }

                                int quoteid = Convert.ToInt32(reader["id"]);
                                DateTime dateAdded = reader.GetDateTime(1); // if the DB changes, use getOrdinal()
                                string author = reader["author"].ToString();
                                string quote = reader["quote"].ToString();
                                EmbedBuilder eb = new EmbedBuilder();
                                string[] userparts = author.Split("#");
                                var authorid = Context.Client.GetUser(userparts[0], userparts[1]);

                                if (authorid is null)
                                    eb.WithAuthor(new EmbedAuthorBuilder().WithName(author));
                                else
                                    eb.WithAuthor(authorid);

                                /* convert quote into discord-compatible string */

                                eb.WithTitle($"Quote #{id}");
                                eb.WithDescription(quote);
                                eb.WithTimestamp(new DateTimeOffset(dateAdded).AddHours(-5)); // force EST for now, come back to this later
                                eb.WithColor(Color.Blue);
                                eb.WithFooter(footer => footer.Text = "Quote saved");
                                Embed embed = eb.Build();

                                await Context.Channel.SendMessageAsync(null, false, embed);
                            }
                        }
                        catch (Exception e)
                        {
                            await Program.raDebugChannel.SendMessageAsync($"Quotebot Exception thrown: {e.Message}");
                            await Context.Channel.SendMessageAsync($"Looks like something went wrong, try again in a few moments.");
                        }
                    }
                }
            }
        }

        [Command("qadd")]
        [Summary("Adds a quote to the database")]
        public async Task AddQuote([Remainder] string quote)
        {
            if (quote.Length < 1)
                return;

            using (Context.Channel.EnterTypingState())
            {
                using (MySqlConnection conn = new MySqlConnection(config.mysqlConnStr))
                {
                    await conn.OpenAsync();

                    using (MySqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "INSERT INTO ralpha.alphabot_quotes (added, author, quote) VALUES (NOW(), @author, @quote)";
                        cmd.Parameters.AddWithValue("@author", Context.Message.Author.ToString());
                        cmd.Parameters.AddWithValue("@quote", quote);
                        cmd.Prepare();

                        try
                        {
                            await cmd.ExecuteNonQueryAsync();
                            await Context.Channel.SendMessageAsync($"Quote #{cmd.LastInsertedId} added to database!");
                        }
                        catch(Exception e)
                        {
                            await Program.raDebugChannel.SendMessageAsync($"Quotebot Exception thrown: {e.Message}");
                            await Context.Channel.SendMessageAsync($"Looks like something went wrong, try again in a few moments.");
                        }
                    }
                }
            }
        }

        [AdminOnly]
        [Command("qdel")]
        [Summary("Deletes a quote from the database")]
        public async Task DelQuote(int id)
        {
            if (id < 1)
                return;

            using (Context.Channel.EnterTypingState())
            {
                using (MySqlConnection conn = new MySqlConnection(config.mysqlConnStr))
                {
                    await conn.OpenAsync();

                    using (MySqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = $"DELETE FROM ralpha.alphabot_quotes WHERE id = {id}";

                        try
                        {
                            int delcount = 0;

                            await Task.Run(() => delcount = cmd.ExecuteNonQuery()); // hacky solution
                            await Context.Channel.SendMessageAsync(delcount > 0 ? $"Quote #{id} deleted!" : $"Quote #{id} does not exist.");
                        }
                        catch (Exception e)
                        {
                            await Program.raDebugChannel.SendMessageAsync($"Quotebot Exception thrown: {e.Message}");
                            await Context.Channel.SendMessageAsync($"Looks like something went wrong, try again in a few moments.");
                        }
                    }
                }
            }
        }

        [Command("roll")]
        [Summary("rolls a NdN die")]
        public async Task RollDie(string die)
        {
            string[] dieparts = die.Split("d");

            if(dieparts.Length < 2)
            {
                await Context.Channel.SendMessageAsync("Dice roll must be in NdN format - ex 1d4");
                return;
            }

            int dieCount = Convert.ToInt32(dieparts[0]);
            int dieSize = Convert.ToInt32(dieparts[1]);

            if(dieCount < 1 || dieSize < 2 || dieCount > 20 || dieSize > 100)
            {
                await Context.Channel.SendMessageAsync("Dice roll sizes must be between 1d2 and 20d100");
                return;
            }

            List<int> rollResults = new List<int>();
            int dieTotal = 0;
            int roll = 0;
            Random rnd = new Random();

            for(int i = 0; i < dieCount; i++)
            {
                roll = rnd.Next(1, dieSize);
                dieTotal += roll;

                rollResults.Add(roll);
            }

            string results = "";
            int rr = 0;
            //string box = "";

            if(rollResults.Count > 1)
            {
                for (int rn = 0; rn < rollResults.Count; rn++)
                {
                    rr = rollResults[rn];

                    /*if (rr == 1)
                        box = "\u1F7E5";
                    else if (rr == dieSize)
                        box = "\u1F7E9";
                    else
                        box = "\uD83D\uDFE6";*/

                    if (rn == 0)
                        results += $"{rr} "; // \u20E3 for enclosing numbers between 0 and 9
                    else
                    {
                        results += $"+ {rr} ";
                    }
                }

                results += $"= {dieTotal}";
            }
            else
                results = $"{dieTotal}";

            await Context.Channel.SendMessageAsync(results);
        }

        [Command("roonline")]
        [Summary("Displays a listing of all online characters in Radiant Renewal RO")]
        public async Task ROOnlinePoll()
        {
            string onlineJSON = null;

            using (Context.Channel.EnterTypingState())
            {
                try
                {
                    onlineJSON = await webClient.GetStringAsync("http://radiantalpha.ddns.net/rostuff/alphabotapi.php");
                }
                catch (Exception e)
                {
                    await Program.raDebugChannel.SendMessageAsync($"Exception thrown from roonline: {e.Message}");
                    await Context.Channel.SendMessageAsync($"Looks like something went wrong, try again in a few moments.");
                    return;
                }

                var online = JsonConvert.DeserializeObject<RROOnlineData>(onlineJSON);

                if(!online.success)
                {
                    await Context.Channel.SendMessageAsync("The server is unavailable for some reason.");
                    return;
                }

                EmbedBuilder roRenewalEmbed = new EmbedBuilder();
                roRenewalEmbed.WithAuthor(Context.Client.CurrentUser);

                if(online.online.Count > 0)
                {
                    roRenewalEmbed.WithTitle("Users online in Radiant Renewal RO");

                    foreach (var user in online.online)
                        roRenewalEmbed.AddField(user.char_name, $"Lv{user.base_level}/{user.job_level} {user.char_class} - Guild: {user.guild_name} - Last seen at {user.last_map}");
                }
                else
                    roRenewalEmbed.WithTitle("Currently no users logged into Radiant Renewal RO");

                roRenewalEmbed.WithCurrentTimestamp();
                await Context.Channel.SendMessageAsync(null, false, roRenewalEmbed.Build());
            }
        }

    }
}
