﻿using System;
using System.Threading.Tasks;
using System.Linq;
using Discord;
//using Discord.Rest;
using Discord.Commands;
using Discord.WebSocket;
using RadiantAlphaBotCore.Preconditions;

namespace RadiantAlphaBotCore.Modules
{
    public class CoreCommandModule : ModuleBase<SocketCommandContext>
    {
        private readonly DiscordSocketClient _discord;
        private static RadiantAlphaBotConfig config = Program.config;

        public CoreCommandModule(DiscordSocketClient discord)
        {
            _discord = discord;
        }

        /*
         * Sync commands - *technically* not thread safe but who is going to both load from disk _and_ database simultaneously right?!
         */

        [AdminOnly]
        [DevChannelOnly]
        [Command("syncconfig")]
        [Summary("Loads configuration from database")]
        public async Task ResyncConfig()
        {
            using (Context.Channel.EnterTypingState())
            {
                await Task.Run(Program.SyncConfig);
                await Context.Channel.SendMessageAsync("Configuration loaded from database.");
            }
        }

        [AdminOnly]
        [DevChannelOnly]
        [Command("saveconfig")]
        [Summary("Saves configuration to disk")]
        public async Task SerializeConfig()
        {
            using (Context.Channel.EnterTypingState())
            {
                await Task.Run(Program.SaveConfig);
                await Context.Channel.SendMessageAsync("Configuration saved to disk.");
            }
        }

        [AdminOnly]
        [DevChannelOnly]
        [Command("loadconfig")]
        [Summary("Load configuration from disk")]
        public async Task DeserializeConfig()
        {
            using (Context.Channel.EnterTypingState())
            {
                await Task.Run(Program.SaveConfig);
                await Context.Channel.SendMessageAsync("Configuration loaded from disk.");
            }
        }

        [AdminOnly]
        [Command("setrolemessageid")]
        [Summary("Sets the ID of the role reaction message to watch")]
        public async Task UpdateRoleReactionId(ulong msgid)
        {
            if(msgid < 1)
            {
                await Context.Channel.SendMessageAsync($"Invalid role ID {msgid}");
                return;
            }

            Program.config.roleMessageId = msgid;

            await Context.Channel.SendMessageAsync($"Successfully set role message ID to {msgid}");

        }

        [AdminOnly]
        [Command("updaterolemessage", RunMode = RunMode.Async)]
        [Summary("Rebuilds the embed used for the role message")]
        public async Task UpdateRoleMessageEmbed()
        {
            /*var rm = await Context.Guild.GetTextChannel(544005469719494657).GetMessageAsync(555597507262939176); // Program.config.roleMessageId
            //var roleChannel = _discord.GetGuild(config.adminGuildId).GetTextChannel(544005469719494657); // as IMessageChannel;
            var restUser = await _discord.Rest.GetGuildUserAsync(Context.Guild.Id, _discord.CurrentUser.Id);
            var restGuild = await restClient.GetGuildAsync(config.adminGuildId);
            var roleChannel = await restUser. //.GetChannelAsync(544005469719494657);
            var roleMessage = roleChannel.GetMessageAsync(Program.config.roleMessageId, CacheMode.AllowDownload); //  (roleChannel as IMessageChannel)*/

            string desc = "**React below to gain the specified role, or unreact to remove that role.**\n";

            /*EmbedBuilder roleEmbed = new EmbedBuilder();
            roleEmbed.WithTitle("React below to gain the specified role, or unreact to remove that role.");
            //roleEmbed.WithDescription("Protip: React and Unreact to this message if you have the role but not the reaction");
            roleEmbed.WithFooter(footer => footer.WithText("Last updated"));
            roleEmbed.WithCurrentTimestamp();
            roleEmbed.WithColor(new Color(0, 255, 0));*/

            foreach(var role in config.roleMap.Values)
            {
                var emote = Context.Guild.Emotes.FirstOrDefault(x => x.Name.IndexOf(role.emojiname, StringComparison.OrdinalIgnoreCase) != -1);
                string emojId = null;

                if (emote is null)
                    emojId = "0";
                else
                    emojId = emote.ToString();

                //roleEmbed.AddField(emojId + " " + role.roleName, role.description, true);
                desc += $"{emojId} **{role.roleName}**: {role.description}\n";
            }

            //roleEmbed.WithDescription(desc);
            //var rm = roleMessage;
            /*await rm.ModifyAsync(msg =>
            {
                msg.Content = desc;
                //msg.Embed = roleEmbed.Build();
            });*/

            await Context.Channel.SendMessageAsync($"Refreshed role message embed.");
            await Context.Channel.SendMessageAsync(desc);
            //await Context.Channel.SendMessageAsync(null, false, roleEmbed.Build());
        }
    }
}