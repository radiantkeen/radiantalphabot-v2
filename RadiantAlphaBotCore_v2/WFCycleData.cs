﻿using System;
using System.Collections.Generic;

namespace RadiantAlphaBotCore.Warframe
{
    public class WFStatCycleData
    {
        public string id { get; set; }
        public DateTime expiry { get; set; }
        public string state { get; set; }
        public DateTime activation { get; set; }
        public string timeleft { get; set; }
        public string shortString { get; set; }
    }

    public class WFStatCycleDataEarth : WFStatCycleData
    {
        public bool isDay { get; set; }
        public bool isCetus { get; set; }
    }

    public class WFStatCycleDataVenus : WFStatCycleData
    {
        public bool isWarm { get; set; }
    }
}
