﻿using System;
using System.Collections.Generic;

namespace RadiantAlphaBotCore
{
    public class RadiantAlphaBotConfig
    {
        public string emoteFilePath { get; set; }
        public string mysqlConnStr { get; set; }
        public bool debugMode { get; set; }
        public string botListenChar { get; set; }
        public ulong adminGuildId { get; set; }
        public string authKey { get; set; }
        public ulong roleMessageId { get; set; }
        public string lavalinkIPAddr { get; set; }
        public string lavalinkPassword { get; set; }
        public Dictionary<string, RABotRoleMapping> roleMap { get; set; }
        public Dictionary<string, ulong> specialChannel { get; set; }
        public Dictionary<string, string> emoteList { get; set; }

        public RadiantAlphaBotConfig()
        {
            emoteFilePath = "http://my.emote.library.com/"; // trailing / is important
            debugMode = true;
            botListenChar = "~";
            authKey = "This space intentionally left blank";
            mysqlConnStr = "Test connection string, please ignore";
            lavalinkIPAddr = "127.0.0.1";
            lavalinkPassword = "enter your lavatunes password here";
            adminGuildId = 0;
            roleMessageId = 0;
        }
    }
}
